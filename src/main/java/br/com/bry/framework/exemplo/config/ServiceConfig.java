package br.com.bry.framework.exemplo.config;

public class ServiceConfig {

	public static final String URL_TIMESTAMP_SERVER = "https://fw2.bry.com.br/api/carimbo-service/v1/timestamps";

	public static final String ACCESS_TOKEN = "<INSERT_VALID_ACCESS_TOKEN>";

}
