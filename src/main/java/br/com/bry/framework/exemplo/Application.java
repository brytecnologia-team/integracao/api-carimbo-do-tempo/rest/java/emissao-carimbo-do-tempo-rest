package br.com.bry.framework.exemplo;

import br.com.bry.framework.exemplo.stamp.Stamp;

public class Application {

	/**
	 * Run the application
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Stamp stamp = new Stamp();

		System.out.println(
				"=====================================Starting timestamp by document...=====================================");

		// The following method will stamp the configured content
		stamp.toStampContent();

		System.out.println();

		System.out.println(
				"=====================================Starting timestamp by document hash ...=====================================");

		// The following method will stamp the configured content hash.
		stamp.toStampDocumentHash();

	}
}
