package br.com.bry.framework.exemplo.stamp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.Base64;

import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.tsp.TSPException;
import org.bouncycastle.tsp.TimeStampToken;
import org.bouncycastle.util.encoders.Hex;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.bry.framework.exemplo.config.ServiceConfig;
import br.com.bry.framework.exemplo.config.TimeStampConfig;
import br.com.bry.framework.exemplo.util.ConverterUtil;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;

public class Stamp {

	private String token = ServiceConfig.ACCESS_TOKEN;

	private static final String URL_TIMESTAMP_SERVER = ServiceConfig.URL_TIMESTAMP_SERVER;

	private ConverterUtil converterUtil = new ConverterUtil();

	/**
	 * Stamp the configured content.
	 */
	public void toStampContent() {
		try {

			// Validates the access token
			this.validateAccessToken();

			Object verifierResponse = null;

			// Beginning of the stamp request configuration
			RequestSpecBuilder builder = new RequestSpecBuilder();

			int timestampQuantitiesToVerify = 1;

			for (int indexOfTimeStamp = 0; indexOfTimeStamp < timestampQuantitiesToVerify; indexOfTimeStamp++) {
				builder.addMultiPart("documents[" + indexOfTimeStamp + "][nonce]",
						Integer.toString(TimeStampConfig.NONCE_OF_TIMESTAMP));
				builder.addMultiPart("documents[" + indexOfTimeStamp + "][content]",
						new File(TimeStampConfig.DOCUMENT_PATH));
			}

			builder.addMultiPart("nonce", String.valueOf(TimeStampConfig.NONCE));
			builder.addMultiPart("hashAlgorithm", TimeStampConfig.HASH_ALGORITHM);
			builder.addMultiPart("format", TimeStampConfig.FORMAT_FILE);

			RequestSpecification requestSpec = builder.build();
			// End of stamp request configuration

			// The next instruction performs communication with the API to stamp the content
			verifierResponse = RestAssured.given().auth().preemptive().oauth2(token).spec(requestSpec).expect()
					.statusCode(200).when().post(URL_TIMESTAMP_SERVER).as(Object.class);

			// Convert from answer object to json
			String jsonResponse = this.converterUtil.convertObjectToJSON(verifierResponse);

			System.out.println("JSON response of time stamp verification: " + jsonResponse);

			// Gets the timeStamps json array
			JSONArray timeStamps = new JSONObject(jsonResponse).getJSONArray("timeStamps");

			// Prints the stamp report
			this.timeStampReport(timeStamps);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Stamp the configured content hash.
	 */
	public void toStampDocumentHash() {
		try {

			// Validates the access token
			this.validateAccessToken();

			Object verifierResponse = null;

			// Beginning of the stamp request configuration
			RequestSpecBuilder builder = new RequestSpecBuilder();

			int timestampQuantitiesToVerify = 1;

			File file = new File(TimeStampConfig.DOCUMENT_PATH);
			// Init array with file length
			byte[] bytesArray = new byte[(int) file.length()];

			FileInputStream fis = new FileInputStream(file);
			fis.read(bytesArray); // read file into bytes[]
			fis.close();

			// Calculates SHA256 hash
			MessageDigest md;
			md = MessageDigest.getInstance("SHA-256");
			md.update(bytesArray);

			// Encodes the calculated hash in hexadecimal format
			String fileHash = new String(Hex.encode(md.digest()));

			for (int indexOfTimeStamp = 0; indexOfTimeStamp < timestampQuantitiesToVerify; indexOfTimeStamp++) {
				builder.addMultiPart("documents[" + indexOfTimeStamp + "][nonce]",
						Integer.toString(TimeStampConfig.NONCE_OF_TIMESTAMP));
				builder.addMultiPart("documents[" + indexOfTimeStamp + "][content]", fileHash);
			}

			builder.addMultiPart("nonce", String.valueOf(TimeStampConfig.NONCE));
			builder.addMultiPart("hashAlgorithm", TimeStampConfig.HASH_ALGORITHM);
			builder.addMultiPart("format", TimeStampConfig.FORMAT_HASH);

			RequestSpecification requestSpec = builder.build();
			// End of stamp request configuration

			// The next instruction performs communication with the API to stamp the content
			// hash
			verifierResponse = RestAssured.given().auth().preemptive().oauth2(token).spec(requestSpec).expect()
					.statusCode(200).when().post(URL_TIMESTAMP_SERVER).as(Object.class);

			// Convert from answer object to json
			String jsonResponse = this.converterUtil.convertObjectToJSON(verifierResponse);

			System.out.println("JSON response of time stamp verification: " + jsonResponse);

			// Gets the timeStamps json array
			JSONArray timeStamps = new JSONObject(jsonResponse).getJSONArray("timeStamps");

			// Prints the stamp report
			this.timeStampReport(timeStamps);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Validates the configured access token
	 * 
	 * @throws Exception
	 */
	private void validateAccessToken() throws Exception {
		if (this.token.equals("<INSERT_VALID_ACCESS_TOKEN>")) {
			throw new Exception("Set up a valid token!");
		}
	}

	/**
	 * Prints the stamp report
	 * 
	 * @param timeStamps
	 * @throws JSONException
	 * @throws CMSException 
	 * @throws IOException 
	 * @throws TSPException 
	 */
	private void timeStampReport(JSONArray timeStamps) throws JSONException, CMSException, TSPException, IOException {

		int numberOfTimeStampsDone = timeStamps.length();

		System.out.println();

		System.out.println("Amount timestamps realized: " + numberOfTimeStampsDone);

		System.out.println();

		JSONObject selectedTimeStamp = new JSONObject(timeStamps.getString(0));

		System.out.println("Nonce of timestamp: " + selectedTimeStamp.getString("nonce"));

		System.out.println();

		System.out.println("Encoded timestamp in base64: " + selectedTimeStamp.get("content"));
		
		System.out.println();

		// The following is an example of how we can obtain information (in which case
		// the date and time were obtained) from the content response in base64 using
		// the bouncy castle library.
		byte[] timeStampTokenBinary = Base64.getDecoder().decode(selectedTimeStamp.get("content").toString());

		CMSSignedData cmsSignedData = new CMSSignedData(timeStampTokenBinary);

		TimeStampToken timeStampToken = new TimeStampToken(cmsSignedData);

		System.out.println("Date and time timestamp: " + timeStampToken.getTimeStampInfo().getGenTime());

	}

}
