package br.com.bry.framework.exemplo.config;

import java.io.File;

public class TimeStampConfig {

	private static final String PATH_SEPARATOR = File.separator;

	// Request identifier
	public static final int NONCE = 1;

	// Identifier of the timestamp within a batch
	public static final int NONCE_OF_TIMESTAMP = 1;

	// Available values: "SHA1", "SHA256" e "SHA512".
	public static final String HASH_ALGORITHM = "SHA256";

	// format type
	public static final String FORMAT_HASH = "HASH";

	// format type
	public static final String FORMAT_FILE = "FILE";

	// location where the document is stored
	public static final String DOCUMENT_PATH = System.getProperty("user.dir") + "" + PATH_SEPARATOR + "src"
			+ PATH_SEPARATOR + "main" + PATH_SEPARATOR + "resources" + PATH_SEPARATOR + "documento" + PATH_SEPARATOR
			+ "image.png";

}
