# Emissão de Carimbo do Tempo

Este é um exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia Java para emissão de carimbo do tempo. 

### Tech

O exemplo utiliza as bibliotecas Java abaixo:
* [JSON] - JSON is a light-weight, language independent, data interchange format
* [RestAssured] - Testing and validating REST client in Java
* [JDK 8] - Java 8

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastra-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.


**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente à emissão do carimbo do tempo.

| Variável | Descrição | Classe de Configuração |
| ------ | ------ | ------ |
| ACCESS_TOKEN | Access Token para o consumo do serviço (JWT). | ServiceConfig


### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência (utilizamos o Eclipse).

Executar programa:

	clique com o botão direito em cima do arquivo Application.java -> Run as -> Java Application

   [RestAssured]: <http://rest-assured.io/>
   [JSON]: <https://github.com/douglascrockford/JSON-java>
   [JDK 8]: <https://www.oracle.com/java/technologies/javase-jdk8-downloads.html>
